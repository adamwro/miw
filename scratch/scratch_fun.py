import numpy as np

arr = np.arange(4)
print(arr.shape)
print(arr)

# make it as row vector by inserting an axis along first dimension
row_vec = arr[np.newaxis, :]
print(row_vec.shape)
print(row_vec)

# make it as column vector by inserting an axis along second dimension
col_vec = arr[:, np.newaxis]
print(col_vec.shape)
print(col_vec)

print(np.zeros((5,5)))

print(np.zeros((2,5)).reshape((5,2)).flatten())