import numpy as np
from sklearn import datasets
from matplotlib import pyplot

iris = datasets.load_iris()
"""
ir = pd.DataFrame(iris.data)
ir.columns = iris.feature_names
ir['CLASS'] = iris.target

x = ir.drop(['CLASS'], axis=1)
y = ir['CLASS']
half = int(len(x) / 2)

training_data_x = x[:half]
training_data_y = y[:half]

testing_data_x = x[half:]
testing_data_y = y[half:]
"""

x = iris.data
y = iris.target
half = int(len(x) / 2)

training_data_x = x[:half]
training_data_y = y[:half]

testing_data_x = x[half:]
testing_data_y = y[half:]
"""
x1 = np.asarray(training_data_x).T
y2 = np.asarray(training_data_y).T
"""

c2 = np.hstack([training_data_x, np.ones(training_data_x.shape)])
c3 = np.hstack([testing_data_x, np.ones(testing_data_x.shape)])
d = np.linalg.pinv(c2.T.dot(c2)).dot(c2.T.dot(training_data_y))
print(d)
predict_training_data = c2.dot(d)
predict_testing_data = c3.dot(d)
print(predict_testing_data)


blad = 1 - ((75 * sum(np.square((predict_testing_data - testing_data_y)))) / (
        75 * sum(np.square((np.mean(testing_data_y) - testing_data_y)))))
print(blad)
"""
print(iris.data)
x = iris.data
y = iris.target
half = len(x)/2

training_data_x = x[:half]
training_data_y = y[:half]

testing_data_x = x[half:]
testing_data_y = y[half:]
"""