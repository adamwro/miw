
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cross_validation import train_test_split
a = np.loadtxt('dane8.txt')

x = a[:,[0]]
y = a[:,[1]]

# assign x and y to separate training and test sets
xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.25)

c1 = np.hstack([xtrain*3, np.ones(xtrain.shape)])
v1 = np.linalg.pinv(c1) @ ytrain   # model 1 y=3ax+b

plt.plot(xtest, ytest, 'ro')
plt.plot(xtest,v1[0]*xtest*3 + v1[1])
plt.show()

# calculate MSE
mse1 = sum((ytest - (v1[0]*xtest*3  + v1[1])) * (ytest - (v1[0]*xtest*3  + v1[1])))
print('Mean square error: ', mse1)


# MODEL 2
c2 = np.hstack([1/(2*xtrain), np.ones(xtrain.shape)])   # model 2 y=a/2x+b
v2 = np.linalg.pinv(c2) @ ytrain

plt.plot(xtest, ytest, 'ro') 
plt.plot(xtest, v2[0]/(2*xtest) + v2[1])
plt.show()

# calculate MSE
mse2 = sum((ytest - (v2[0]/(2*xtest)  + v2[1])) * (ytest - (v2[0]/(2*xtest)  + v2[1])))
print('Mean square error: ', mse2)


# MODEL 3
c3 = np.hstack([xtrain*xtrain*xtrain*xtrain, xtrain*xtrain, xtrain, np.ones(xtrain.shape)])   # model 2 y=a/x+b
v3 = np.linalg.pinv(c3) @ ytrain

plt.plot(xtest, ytest, 'ro') 
plt.plot(xtest,v3[0]*xtest*xtest*xtest*xtest + v3[1]*xtest*xtest + v3[2]*xtest + v3[3])
plt.show()

# calculate MSE
mse3 = sum((ytest - (v3[0]*xtest*xtest*xtest*xtest + v3[1]*xtest*xtest + v3[2]*xtest + v3[3])) 
    * ((ytest - (v3[0]*xtest*xtest*xtest*xtest + v3[1]*xtest*xtest + v3[2]*xtest + v3[3]))))
print('Mean square error: ', mse3)
# =============================================================================
# # calculate MSE
# mse1 = sum((y - (v1[0]*x  + v1[1])) * (y - (v1[0]*x  + v1[1])))
# print('Mean square error: ', mse1)
# 
# c2 = np.hstack([1/x, np.ones(x.shape)])   # model 2 y=a/x+b
# v2 = np.linalg.pinv(c2) @ y
# 
# plt.plot(x, y, 'ro')
# #plt.plot(x,v[0]*x*x*x + v[1]*x*x + v[2]*x + v[3],)
# plt.plot(x,v1[0]/x + v1[1])
# plt.plot(x,v1[0]*x + v1[1])
# plt.show()
# 
# =============================================================================

