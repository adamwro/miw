import numpy as np
import matplotlib.pyplot as plt

# Dane
a = np.loadtxt('Dane/Szkoleniowe.txt')
# print(a)

# Oś Y
y = a[:, [1]]
# print(x)

# Oś X
x = a[:, [0]]
# print(y)

# Zbudowanie macierzy [x, 1]
c2 = np.hstack([x, np.ones(x.shape)])
# print(c2)

# Wyliczenie wartości w1 i w0 na podstawie wcześniejszej macierzy
v2 = np.linalg.pinv(c2) @ y
sumaBledu2 = sum((y - (2*(v2[0] * x + v2[1]))))/len(y)
print("test")
print(sumaBledu2)
sumaBledu = v2[0] + v2[1]
print(v2)
print(sumaBledu)
# Model 1
# struktura modelu: Liniowa
# dane treningowe: Szkoleniowe.txt
# dane testujące: Testowe.txt
# wartość oczekiwana błąd modelu :
# dla danych treningowych: 0.75876793
# wartość oczekiwana błąd modelu :
# dla danych testujących: 0.67911304
# ocena modelu: Dobra
# Inne uwagi: Brak
# -------------------------------------------
c = np.hstack([x*x*x, x*x, x, np.ones(x.shape)])
# print(c)
v = np.linalg.pinv(c) @ y
# print(v)

sumaBledu2 = v[0] + v[1] + v[2] + v[3]
print(sumaBledu2)
# Model 2
# struktura modelu: Nieliniowa
# dane treningowe: Szkoleniowe.txt
# dane testujące: Testowe.txt
# wartość oczekiwana błąd modelu :
# dla danych treningowych: 4.06544967
# wartość oczekiwana błąd modelu :
# dla danych testujących: 3.97094213
# ocena modelu: Dobra
# Inne uwagi: Brak


# Reprezentacja graficzna
plt.plot(x, y, 'ro')
plt.plot(x, v2[0]*x + v2[1])
plt.plot(x, v[0]*x*x*x + v[1]*x*x + v[2]*x + v[3], 'b-')
plt.show()
