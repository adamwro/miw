# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from typing import List, Dict, Tuple

print("hello world")

a = 1
b = "1"
c = 1.5
d = True
e = False
f = [1, 2, 3]
g = (1, 2, 3)
h = 1, 2, 3
i = {1, 2, 3}
j = {"a": 1, "b": 2}
k, l, m = 1, 2, 3

print(h)


class A:
    __a: int

    # __ -> private
    # _ -> protected
    # c: bool -> public

    def __init__(self, a: int = 1):
        self.__a = a

    @staticmethod
    def a():
        print("a")

    @classmethod
    def b(cls):
        cls.a()


A.b()

A()

if 1 == 2:
    print("?")
elif 2 == 3:
    print("?" * 20)
else:
    print("!" * 30)

a = 1 if True else 2

a = [1, 2, 3]
b = [4, 3, 2]

print((a + b) + [0])

some_numbers: List[int] = [None] * len(a)

print(some_numbers)

print([i for i in range(11) if i % 2 == 0])

generator = (i for i in range(11) if i % 2 == 0)

try:
    while True:
        print(next(generator))
except StopIteration:
    pass
generator.close()
print(dir(generator))


def f():
    raise NotImplementedError


try:
    f()
except NotImplementedError:
    print("NonImplementedError")
except BaseException:
    print("Base")

with open("file.txt", "r") as f:
    print(f.read())


