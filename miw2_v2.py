# https://www.ritchieng.com/machine-learning-iris-dataset/
# https://medium.com/edureka/linear-regression-in-python-e66f869cb6ce
# http://www2.lawrence.edu/fast/GREGGJ/Python/numpy/numpyLA.html
# https://medium.com/@randerson112358/python-logistic-regression-program-5e1b32f964db
# https://pythonprogramming.net/how-to-program-r-squared-machine-learning-tutorial/
# import this
# check np.linalg.lstsq()
from sklearn.datasets import load_iris
import numpy
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

iris = load_iris()

X = iris.data
Y = iris.target

print(f"X = {X}")
print(f"X = {Y}")


def y_to_names(target):
    return list(map(lambda x: iris.target_names[x], target))


# Plot the relation of each feature / column with species.
# Scatter plot to show this relation
def plot_relation(x, y):
    plt.xlabel('Features')
    plt.ylabel('Species')

    pltX = x[:, 0]  # data.loc[:, 'sepal_length']
    pltY = y  # iris.target_names #  data.loc[:,'species']
    plt.scatter(pltX, pltY, color='blue', label=iris.feature_names[0])

    pltX = x[:, 1]  # data.loc[:, 'sepal_width']
    pltY = y  # iris.target_names #  data.loc[:,'species']
    plt.scatter(pltX, pltY, color='green', label=iris.feature_names[1])

    pltX = x[:, 2]  # data.loc[:, 'petal_length']
    pltY = y  # iris.target_names # data.loc[:,'species']
    plt.scatter(pltX, pltY, color='red', label=iris.feature_names[2])

    pltX = x[:, 3]  # data.loc[:, 'petal_width']
    pltY = y  # iris.target_names # data.loc[:,'species']
    plt.scatter(pltX, pltY, color='black', label=iris.feature_names[3])

    plt.legend(loc=4, prop={'size': 8})
    plt.show()


plot_relation(iris.data, y_to_names(iris.target))

X = np.hstack((X, np.ones((150, 1))))

# Split the data into 70% training and 30% testing
x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.3)

print(f"x_train = {x_train}")
print(f"x_test = {x_test}")
print(f"y_train = {y_train}")
print(f"y_test = {y_test}")

plot_relation(x_train, y_to_names(y_train))
plot_relation(x_test, y_to_names(y_test))

# (Xt * X)^-1*Xt*y = (Xt * X)beta = Xt * yz
Xt = np.transpose(X)
XtX = Xt @ X
Xty = Xt @ Y
beta = np.linalg.solve(XtX, Xty)
print(f"beta = {beta}")

model_prediction = x_test @ beta

model_error = 1 - (
        (sum(np.square((model_prediction - y_test)))) / (sum(np.square((np.mean(y_test) - y_test))))
)

print(f"model_error = {model_error}")


def best_fit_slope_and_intercept(xs, ys):
    m = (((np.mean(xs) * np.mean(ys)) - np.mean(xs @ numpy.array([ys, ys, ys, ys, ys]))) /
         ((np.mean(xs) * np.mean(xs)) - np.mean(numpy.transpose(xs) @ xs)))
    b = np.mean(ys) - m * np.mean(xs)
    return m, b

m, b = best_fit_slope_and_intercept(x_train, y_train)
regression_line = [(m * x) + b for x in x_train]

plt.scatter(X, numpy.array([Y, Y, Y, Y, Y]), color='red', label='data')
plt.plot(x_train, regression_line, label='regression line')
# plt.legend(loc=4)
# plt.show()

# Polynomial Regression
def polyfit(x, y, degree):
    results = {}

    coeffs = numpy.polyfit(x, y, degree)

    # Polynomial Coefficients
    results['polynomial'] = coeffs.tolist()

    # r-squared
    p = numpy.poly1d(coeffs)
    # fit values, and mean
    yhat = p(x)                         # or [p(z) for z in x]
    ybar = numpy.sum(y)/len(y)          # or sum(y)/len(y)
    ssreg = numpy.sum((yhat-ybar)**2)   # or sum([ (yihat - ybar)**2 for yihat in yhat])
    sstot = numpy.sum((y - ybar)**2)    # or sum([ (yi - ybar)**2 for yi in y])
    results['determination'] = ssreg / sstot

    return results
