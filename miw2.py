# https://www.ritchieng.com/machine-learning-iris-dataset/
# https://medium.com/edureka/linear-regression-in-python-e66f869cb6ce
# import this
from sklearn.datasets import load_iris
import numpy
import numpy as np
import matplotlib.pyplot as plt

iris = load_iris()
iris

X = iris.data
Y = iris.target

print(f"X = {X}")

# shape (n, 1) - column vector
# shape (1, n) - row vector
# create column vector from labels
# y = iris.target[:, np.newaxis]
# x = iris.data
#
# print(iris.target)
print(Y)

print("Total samples in our dataset is: {}".format(X.shape[0]))
# print(f"data length {X}")

mean_x = np.mean(X)
mean_y = np.mean(Y)

n = len(X)

number = 0
denom = 0
for i in range(n):
    number += (X[i] - mean_x) * (Y[i] - mean_y)
    denom += (X[i] - mean_x) ** 2
m = number / denom
c = mean_y - (m * mean_x)

print(f"m = {m}")
print(f"c = {c}")


tmp = []
for i in range(len(Y)):
    tmp.append(Y[i])
    tmp.append(Y[i])
    tmp.append(Y[i])
    tmp.append(Y[i])

print(f"tmp: {tmp}")

max_x = np.max(X) + 10
min_x = np.min(X) + 10
x1 = np.linspace(min_x, max_x, 100)
y1 = np.mean(c) + np.mean(m) * x1
# y1 = b0 + b1 * x1

print(x1)
print(y1)



plt.plot(X, Y, color='#58b970', label='asdf')
plt.show()
print(X)
print(Y)
plt.scatter(X.flatten(), tmp)
plt.show()


# def compute_cost(X, y, params):
#     n_samples = len(y)
#     h = X @ params
#     return (1/(2*n_samples))*np.sum((h-y)**2)
#
#
# def gradient_descent(X, y, params, learning_rate, n_iters):
#     n_samples = len(y)
#     J_history = np.zeros((n_iters,1))
#
#     for i in range(n_iters):
#         params = params - (learning_rate/n_samples) * X.T @ (X @ params - y)
#         J_history[i] = compute_cost(X, y, params)
#
#     return (J_history, params)
#
# n_samples = len(x)
#
# mu = np.mean(X, 0)
# sigma = np.std(X, 0)
#
# X = (X-mu) / sigma
#
# X = np.hstack((np.ones((n_samples,1)),X))
# n_features = np.size(X,1)
# params = np.zeros((n_features,1))
#
#
# n_iters = 1500
# learning_rate = 0.01
#
# initial_cost = compute_cost(X, y, params)
#
# print("Initial cost is: ", initial_cost, "\n")
#
# (J_history, optimal_params) = gradient_descent(X, y, params, learning_rate, n_iters)
#
# print("Optimal parameters are: \n", optimal_params, "\n")
#
# print("Final cost is: ", J_history[-1])
#
# plt.plot(range(len(J_history)), J_history, 'r')
#
# plt.title("Convergence Graph of Cost Function")
# plt.xlabel("Number of Iterations")
# plt.ylabel("Cost")
# plt.show()

def covariance(X, Y):
    xhat = np.mean(X)
    yhat = np.mean(Y)
    epsilon = 0
    for x, y in zip(X, Y):
        epsilon = epsilon + (x - xhat) * (y - yhat)
    return epsilon / (len(X) - 1)
