import unittest

from miw1 import MatrixWrapper


class TextMatrix(unittest.TestCase):

    def test_multiply(self):
        X = [[1, 2, 3],
             [4, 5, 6],
             [7, 8, 9]]
        Y = [[10, 11, 12, 13],
             [14, 15, 16, 17],
             [18, 19, 20, 21]]
        expected_result = [[92, 98, 104, 110], [218, 233, 248, 263], [344, 368, 392, 416]]
        self.assertEqual(expected_result, MatrixWrapper(X).multiply(Y), "Should be equal")

    def test_transpose(self):
        X = [[11, 12],
             [13, 14],
             [15, 16]]
        expected_result = [[11, 13, 15], [12, 14, 16]]
        self.assertEqual(expected_result, MatrixWrapper(X).transpose(), "Should be equal")

    def test_scalar_multiply(self):
        X = [[11, 12],
             [13, 14],
             [15, 16]]
        factor = 2
        expected_result = [[22, 24], [26, 28], [30, 32]]
        self.assertEqual(expected_result, MatrixWrapper(X).scalar_multiply(factor), "Should be equal")

    def test_dot_product(self):
        X = [[1, 2, 3],
             [4, 5, 6],
             [7, 8, 9]]
        Y = [[10, 11, 12],
             [14, 15, 16],
             [18, 19, 20]]
        expected_result = 753
        self.assertEqual(expected_result, MatrixWrapper(X).dot_product(Y), "Should be equal")

    def test_dot_product2(self):
        X = [[1, 2, 3],
             [4, 5, 6],
             [7, 8, 9]]
        Y = [[10, 11, 12],
             [14, 15, 16],
             [18, 19, 20]]
        expected_result = 753
        product = 0

        a = list(zip(X,Y))

        for i in range(len(a)):
            for j in range(len(a[i][0])):
                product += a[i][0][j]*a[i][1][j]

        print(a)
        print(product)


        self.assertEqual(expected_result, product, "Should be equal")


if __name__ == '__main__':
    unittest.main()
