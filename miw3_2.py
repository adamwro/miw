from sklearn.datasets import load_boston
import numpy as np
from sklearn.model_selection import train_test_split

boston_data = load_boston()


def split_data(x, y):
    arr_rand = np.random.rand(x.shape[0])
    split = arr_rand < np.percentile(arr_rand, 70)
    x_train1 = x[split]
    y_train1 = y[split]
    x_test1 = x[~split]
    y_test1 = y[~split]
    return x_train1, y_train1, x_test1, y_test1


X = boston_data.data
Y = boston_data.target

n, m = X.shape

X = np.hstack((X, np.ones((n, 1))))

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.3)

Xt = np.transpose(x_train)
XtX = Xt @ x_train
Xty = Xt @ y_train

# Xtx = np.linalg.inv(XtX)
# print(np.dot(Xtx,Xty))
#
theta = np.linalg.solve(XtX, Xty)

print(theta)

prediction = x_test @ theta

model_error = 1 - ((sum(np.square((prediction - y_test)))) / (
    sum(np.square((np.mean(y_test) - y_test)))))
print(f"model error: {model_error}")

# O = (XtX + alpha) * Xt * y
alpha = 0.0001
x_t = np.transpose(x_train)
x_t_x = x_t @ x_train
n1, m1, = x_t_x.shape

identity_matrix = np.identity(n1)
identity_matrix[0] = 0

alpha_identity = alpha @ identity_matrix
x_t_x_alpha = np.add(x_t_x, alpha_identity)
x_t_x_alpha_inv = np.linalg.inv(x_t_x_alpha)
theta_l2 = x_t_x_alpha_inv @ (x_t @ y_train)
print(theta_l2)

prediction_l2 = np.dot(x_test, theta_l2)

model_error = 1 - ((sum(np.square((prediction_l2 - y_test)))) / (
    sum(np.square((np.mean(y_test) - y_test)))))
print(f"model error: {model_error}")