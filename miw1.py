from typing import List


class MatrixWrapper:
    __matrix: List[List[int]]

    def __init__(self, matrix: List[List[int]]):
        MatrixWrapper.validate(matrix)
        self.__matrix: List[List[int]] = matrix

    @staticmethod
    def initialize(rows: int, cols: int):
        result = []
        for i in range(rows):
            result.append([0] * cols)
        return result

    @staticmethod
    def validate(matrix: List[List[int]]):
        for row in matrix:
            if len(row) != len(matrix[0]):
                raise ValueError('Not a matrix!', matrix)

    def multiply(self, matrix: List[List[int]]):
        # Multiplying matrices is possible when number of X columns is equal to number of Y rows
        # cols = len(X[0])
        # rows = len(Y)
        MatrixWrapper.validate(matrix)
        if len(self.__matrix[0]) != len(matrix):
            raise ValueError('Cannot multiply matrixes', self.__matrix, matrix)
        result: List[List[int]] = MatrixWrapper.initialize(len(self.__matrix), len(matrix[0]))
        for i in range(len(self.__matrix)):
            for j in range(len(matrix[0])):
                for k in range(len(matrix)):
                    result[i][j] += self.__matrix[i][k] * matrix[k][j]
        return result

    def transpose(self):
        result = MatrixWrapper.initialize(len(self.__matrix[0]), len(self.__matrix))
        for i in range(len(self.__matrix)):
            for j in range(len(self.__matrix[0])):
                result[j][i] = self.__matrix[i][j]
        return result

    def scalar_multiply(self, factor: int):
        result = MatrixWrapper.initialize(len(self.__matrix), len(self.__matrix[0]))
        for i in range(len(self.__matrix)):
            for j in range(len(self.__matrix[0])):
                result[i][j] = self.__matrix[i][j] * factor
        return result

    def dot_product(self, matrix: List[List[int]]):
        if len(self.__matrix) != len(matrix):
            raise ValueError('Cannot get dot product, matrices need to be the same dimensions', self.__matrix, matrix)
        for i in range(len(self.__matrix)):
            if len(self.__matrix[i]) != len(matrix[i]):
                raise ValueError('Cannot get dot product, matrices need to be the same dimensions', self.__matrix,
                                 matrix)

        product = 0
        for i in range(len(self.__matrix)):
            for j in range(len(self.__matrix[0])):
                product = product + self.__matrix[i][j] * matrix[i][j]

        return product
